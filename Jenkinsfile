  #!groovy
import groovy.json.JsonSlurperClassic

properties([
  parameters([
    string(name: 'environment', defaultValue: 'dt-bdc-na', description: 'Environment to build (test, pt, pl)'),
    string(name: 'cluster', defaultValue: 'dt-lcp', description: 'Cluster on which this service should be deployed (defaults to dt-lcp)')
  ]),
  pipelineTriggers([pollSCM('* * * * *')])
])

node() {
    stage("Checkout Repository") {
        //Checkout repository
       step([$class: 'WsCleanup'])
       checkout scm
    }
    stage("Initialize") {
        // Load HipchatIntegration
        // notifications = load pwd()+"@script/HipchatNotify.groovy"

        //Initialize build parameters
        environment = params.environment.toLowerCase()

        echo "Environment: ${environment}"
        echo "Cluster: ${cluster}"
        imageRegistry = "registry.dev-dit.com/" + "${OPENSHIFT_TENANT_PROJECT_NAME}".split("-")[0] + "-" + "${OPENSHIFT_TENANT_PROJECT_NAME}".split("-")[1] + "-build"
        context = cluster
        serviceName = "virtual-service"
        buildName = "${serviceName}-${environment}"
        buildDir = "build/lib"
        baseUrl = "http://localhost:8080"
        artifactoryUrl= "http://artifactory-oss-${PROJECT_NAME}.${OPENSHIFT_ENV}.dit.com/artifactory"

        server = Artifactory.server "artifactory"
    }
    stage("Build") {
        //Clean build directory
        echo "Clean build directory"
        sh "rm -rf ./${buildDir}"

        sh "/opt/apps/scripts/deployment/virtual_service_deploy.py $environment $baseUrl $buildDir"

        echo "Deploying WAR's to ${artifactoryUrl}"

        def uploadSpec = """{"files": [ { "pattern": "build/lib/*.war", "target": "libs-release-local/virtual-services/${environment}/" } ] }"""
        server.upload(uploadSpec)
    }
    stage("Create Image"){
        def services = readFile(file:'services-to-deploy')
        if (services.length() == 0) {
            error("Could not get services list from services-to-deploy file")
        }
        //Create build image
        sh "oc new-build . --name=$buildName -e ARTIFACTORY_URL=$artifactoryUrl -e DEPLOY_SERVICES=$services -e DEPLOY_ENV=$environment --labels=app=$buildName -n ${PROJECT_NAME} --strategy=docker || true"

        //Add git credentials for image build
        sh "oc set build-secret --source bc/$buildName gitsecret"

        //Add build strategry to the build config
        sh "oc patch bc/$buildName --patch '{\"spec\":{\"source\":{\"git\":{\"ref\":\"master\"}},\"strategy\":{\"type\":\"Docker\",\"dockerStrategy\":{\"from\":{\"kind\":\"ImageStreamTag\",\"namespace\":\"esp-foundation-build\",\"name\":\"tomcat:latest\"},\"noCache\":true}}}}'"

        // start build of service image
        sh "oc start-build $buildName --wait=true -n ${PROJECT_NAME} -e DEPLOY_SERVICES=$services"
    }
    stage("Deploy CI") {
        //Deploy virtual service as a CI build
        deleteApp(serviceName, true, environment)
        createNewApp(serviceName, true, environment)
    }
    stage("CI Startup") {
        //Wait for CI serivce to start up
        sh "./validatedeployment.sh ${serviceName}-ci ${environment} 1.0 ${context}"
    }
    stage("Automated Test") {
        //Perform smoke/functional test virtual service
    }
    stage("Teardown CI") {
        //Teardown CI deployment
        deleteApp(serviceName, true, environment)
    }
    stage("Deploy to Environment") {
        //Deploy to environment
        echo "Deploying to " + environment.toUpperCase()
        deleteApp(serviceName, false, environment)
        createNewApp(serviceName, false, environment)
    }
    stage("Finish") {
        //Finish build process
        step([$class: 'WsCleanup'])
        echo "Build completed"
    }
}

def createNewApp(def appName, def isCI, def env) {
    def serviceName = (isCI) ? "${appName}-ci" : "${appName}"

    echo "Create service: ${serviceName}"
    sh "oc new-app ${imageRegistry}/${appName}-${env}:latest --name=${serviceName} -e MAX_HEAP_SIZE=-Xmx3072m -e MIN_HEAP_SIZE=-Xms64m -e STACK_SIZE=-Xss256k -l app=${serviceName} --insecure-registry=true --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env}"

    echo "Establish readiness and liveness probes"
    sh "oc set probe dc/${serviceName} --readiness --get-url=http://:8080/healthcheck --period-seconds=5 --failure-threshold=10 --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env}"
    sh "oc set probe dc/${serviceName} --liveness --initial-delay-seconds=240 --period-seconds=10 --get-url=http://:8080/healthcheck --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env}"

    echo "Set Resource Limits"
    sh "oc set resources dc/${serviceName} --requests=cpu=50m,memory=3000Mi --limits=cpu=100m,memory=5000Mi --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env}"

    echo "Expose route for ${serviceName} if it does not already exist."
    sh "oc get route/${serviceName} --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env} ||  oc expose service ${serviceName} --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env}"

    echo "Wait for ${serviceName} to become available"
  }

def deleteApp(def appName, def isCI, def env) {
    def serviceName = (isCI) ? "${appName}-ci" : "${appName}"
    try {
        echo "Try to remove ${serviceName} deployment if it's sill running"
        sh "oc delete dc,services,route,is ${serviceName} --context=${context} -n ${OPENSHIFT_TENANT_PROJECT_NAME}-${env} --ignore-not-found=true --grace-period=0 --timeout=10s || true"
    }
    catch (error) {
            echo "Error removing ${serviceName} deployment (maybe it was already removed) \n Continuing with build"
    }
}
