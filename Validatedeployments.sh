#!/bin/bash

service_code=$1
env=$2
version=$3
context=$4
if [ "q${5}" = "q" ];then
  healthcheck="/healthcheck"
else
  healthcheck="$5"
fi


set -x
count=0

until [[ `curl -ksL -H "client-id:Jenkins" -w "%{http_code}" http://${service_code}-${OPENSHIFT_TENANT_PROJECT_NAME}-${env}.${context}.dit.com${healthcheck} -o /dev/null` =~ ^("204"||"200")$ ]] || [ "$count" = 240 ];do
  echo 
  echo '--- sleeping for 2 seconds'
  sleep 2
  let count=$count+1
done

if [ "$count" = 240 ]; then
  echo 'deployment timed out!! FAIL'
  exit 1
else
  echo 'app is up!'
fi