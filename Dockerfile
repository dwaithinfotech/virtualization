FROM esp-foundation-build/tomcat:latest

RUN echo -e "SOURCE_FILE: ${SOURCE_FILE} \n artifactory url: ${ARTIFACTORY_URL} \n deploy environment: ${DEPLOY_ENV} \n services: ${DEPLOY_SERVICES}" \
  && IFS=',' read -ra deployservices <<< "$DEPLOY_SERVICES"; for deployservice in "${deployservices[@]}"; do echo "ADDING $deployservice"; curl -L ${ARTIFACTORY_URL}/libs-release-local/virtual-services/${DEPLOY_ENV}/${deployservice}.war -o "/usr/local/tomcat/webapps/${deployservice}.war"; done \
  && chmod -R ugo+rwx /usr/local/tomcat