supports the creation and deployment of SoapUI mock services into the non-prod environments. The XML definitions for virtual services are stored in a repository called "virtual-services".  Each virtual service will be defined by a separate XML file in the repository and then deployed into a shared virtual services deployment in OpenShift.

See the video at this link for details, but keep in mind that it was created before services were defined in their own XMLs 16. Creating Virtual Services 

Steps to create mock service
clone the virtual-services repo
create a new SOAPUI project, naming it with the service code of the service that is being virtualized
create new Rest Mock Service
add mock action to mock service
add mock response to mock action
Save the file to the appropriate environment folder in the services folder of the repo
Each virtual service will be deployed with a context root of its service code, so while creating the services the resource path should be relative to that.  For example, if the SoapUI project is called cis2-customer.xml, then a service within this project with path "/customer" would be accessed via "/cis2/customer".

Please note that the service xml file will be deployed with a base path of what is listed in the deploy-config.json file. There should be at most only one file under each environment folder that starts with that service code.

Deploying SOAPUI Project
There is a file in the services folder called deploy-config.json that controls which services get deployed in each environment (so that you can add and remove them without having to remove them from git)
Any commits to master will automatically trigger the deploy to the test environment.
Deploying to PT and PL requires running the jenkins job with the correct environment parameter
Using Mock Services
Mock services can be accessed from outside the OpenShift cluster via a route with URL "http://virtual-service-{project name}.dev-lcp.na.dit.com/{service code}/{mock endpoint}" or from inside the cluster (ie by other ESP services in the same environment) via "http://virtual-service:8080/{service code}/{mock endpoint}"